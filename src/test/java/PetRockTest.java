import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PetRockTest {
    private PetRock rocky;

    @BeforeEach
    public void myTestSetup() {
        rocky = new PetRock("Rocky");
    }

    @Test
    void getName() {
        assertEquals("Rocky" , rocky.getName());
    }

    @Test
    void testHappy() {
        assertFalse(rocky.isHappy());
    }

    @Test
    void testHappyAfterPlay() {
        rocky.playWithRock();
        assertTrue(rocky.isHappy());
    }

    @Disabled
    @Test
    void nameFail() {
        assertThrows(IllegalStateException.class, () -> rocky.getHappyMessage());
    }

    @Test
    void name() {
        rocky.playWithRock();
       String msg = rocky.getHappyMessage();
       assertEquals("Im happy!" , msg);
    }

    @Test
    void testFavNumber() {
        assertEquals(69, rocky.getFavNumber());
    }

    @Test
    void emptyNameFail() {
        assertThrows(IllegalArgumentException.class, () -> new PetRock(""));
    }
}